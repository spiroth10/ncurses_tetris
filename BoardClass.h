#ifndef _BOARDCLASS_H
#define _BOARDCLASS_H
#include "TileRec.h"
#include "BlockClass.h"

enum direction {DOWN, LEFT, RIGHT, ROTATE};

class BoardClass
{
    public:
        //constructor
        BoardClass();
       
        BoardClass(int ySizeIn, int xSizeIn);

        BoardClass(const BoardClass &boardIn);
        
        //destructor
        ~BoardClass();
        //public functions
        //const int GetXPos(int piecePos);
        //const int GetYPos(int piecePos);

        //bool IsBlockEmpty(int x, int y);

        //bool CanMove(int x, int y, int pieceId, int rotationState)
        //void deletePossibleLines();
        //bool IsGameOver();
        //gets width of a board from existing boardclass
        size_t GetWidth()const;

        //gets height of a board from existing boardclass
        size_t GetHeight()const;

       //  size_t GetRowsToShift(BlockClass &block)const;

       //  size_t GetColsToShift(BlockClass &block)const;
        //returns a tile from position y, x
        char GetTile(size_t y, size_t x)const;
        //sets a specific tile on the board at y, x. same as ncurses
        void SetTile(char tile, size_t y, size_t x);
        //sets an entire block on the screen starting from y,x upper left
        void SpawnBlock(BlockClass &block,size_t y, size_t x);

        void SetBlock(BlockClass &block);
        //moves a block to a new x/y
        void MoveBlock(BlockClass &block, direction dir);

        void RotateBlock(BlockClass &block);

        bool DoesPieceFit(const BlockClass &block, direction dir);
        //clears the board
        void ClearBoard();
        //removes a block from the board 
        void RemoveBlock(BlockClass &block);

        void LandBlock(BlockClass &block);

        const BoardClass& operator=(const BoardClass &boardIn);
      private:
        //the size of the board
        size_t xSize;
        size_t ySize;
        //typedef TileRec to tileArr
        //typedef char tileArr;
        //pointer from tiles to a 2d array board 
        //array instantiated in code
        std::vector<std::vector<TileRec>> board;
};

#endif 

#ifndef  _PIECES_H
#define  _PIECES_H
#include <array>
#include <vector>
//piece offsets
const std::array<std::array<int,4>,3> OFFSETS =
{
    //used for edge detection to see distance to edge of piece
    //needs further modifiers in blockclass get_offsets() function for
    //certain rotations/pieces...
    //bottom, left,top, right
    {{0, 1, 1, 2}, // I
     // 2, 0, 1, 1
     // 1, 2, 0, 1
    {0, 1, 1, 0}, // O offsets
    {0,1,0,1}, // other piece offsets
    } //Z offsets
    ///1 1 1 0
};
/* I piece offsets in each rotation
 * 1, 0, 1, 3
 * 3. 2, 0, 1
 * 2, 0, 2, 3
 * 3, 1, 0, 1
 */

const std::array<std::array<int,4>,4> OFFSETS_I =
{
    {
        {1,0,1,3},
        {3,-2,0,2},
        {2,0,2,3},
        {3,-1,0,1}
    }
};

const std::array<int,4> OFFSETS_O =
{
        {1,0,0,1}
};

const std::array<std::array<int,4>,4> OFFSETS_OTHER =
{
    {
        {1,0,0,2},
        {2,-1,0,2},
        {2,0,1,2},
        {2,0,0,1}
    }
};


//pieces stored as vectors instead of std::array,
//simply to reduce loops for piece assignments
//because std::array cannot be passed around without
//specifying size at compile time.
const std::vector<std::vector<char>> I_PIECE =
{
    {' ',' ',' ',' '},
    {'I','I','I','I'},//
    {' ',' ',' ',' '},
    {' ',' ',' ',' '}
};
const std::vector<std::vector<char>> O_PIECE =
{

    {'O','O'},
    {'O','O'}
};
        
const std::vector<std::vector<char>> J_PIECE =
{
    {'J',' ',' '},
    {'J','J','J'},//
    {' ',' ',' '}
};

const std::vector<std::vector<char>> L_PIECE =
{
    {' ',' ','L'},
    {'L','L','L'},
    {' ',' ',' '}
};

const std::vector<std::vector<char>> S_PIECE =
{
    {' ','S','S'},
    {'S','S',' '},
    {' ',' ',' '}
    
};

const std::vector<std::vector<char>> T_PIECE =
{
    {' ','T',' '},
    {'T','T','T'},
    {' ',' ',' '}
};

const std::vector<std::vector<char>> Z_PIECE =
{
    {'Z','Z',' '},
    {' ','Z','Z'},//
    {' ',' ',' '}
};
/*
const std::vector<int> I_PIECE =
{
    1,3,5,7
};

const std::vector<int> Z_PIECE =
{
    2,4,5,7
};

const std::vector<int> S_PIECE =
{
    3,5,4,6
};

const std::vector<int>T_PIECE =
{
    3,5,4,7
};

const std::vector<int>L_PIECE =
{
    2,3,5,7
};

const std::vector<int>J_PIECE =
{
    3,5,7,6
};

const std::vector<int>O_PIECE =
{
    2,3,4,5
};
*/
#endif

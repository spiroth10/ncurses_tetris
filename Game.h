#ifndef _GAME_H
#define _GAME_H
#include <random>
#include <memory>
#include <ncurses.h>
#include <chrono>
#include <thread>
#include "BlockClass.h"
#include "BoardClass.h"

class Game
{
    public:
        Game();
        Game(int boardX, int boardY);
        ~Game();
        const BlockClass GetNextBlock();
        const BlockClass GetCurrentBlock();
        int GetScore();
        int GetLevel();
        void StartGame();
     
    private:
        int level;
        int score;
        std::unique_ptr<BoardClass> Board;
        BlockClass CurrentBlock, NextBlock;
        //these have to be standard pointers for ncurses library
        WINDOW * boardWin;
        WINDOW * nextBlockWin;
        int GetRand();
        bool check_can_move(int y, int x, direction dir);
        void AddNextBlock(); 
        void PrintBoard();
        void PrintNextBlock();
};

#endif

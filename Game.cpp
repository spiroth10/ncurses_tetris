#include "Game.h"

Game::Game()
{
    level = 1;
    score = 0;
    Board.reset(new BoardClass(20,10));
    NextBlock =  BlockClass(GetRand());
}

Game::Game(int boardX, int boardY)
{
    level = 1;
    score = 0;
    Board.reset(new BoardClass(boardY,boardX));
    NextBlock = BlockClass(Z);
}
// I O J L S T Z
Game::~Game()
{
}

const BlockClass Game::GetNextBlock()
{
    return NextBlock;
}

const BlockClass Game::GetCurrentBlock()
{
    return CurrentBlock;
}

int Game::GetScore()
{
    return score;
}

int Game::GetLevel()
{
    return level;
}

void Game::StartGame()
{
     int boardHeight = static_cast<int>(Board->GetHeight());
     int boardWidth = static_cast<int>(Board->GetWidth());
     int yMax, xMax, yBeg, xBeg;
     getmaxyx(stdscr, yMax, xMax);
     boardWin = newwin(boardHeight-2, boardWidth+2,(yMax/2)-10, (xMax/2));
     getbegyx(boardWin, yBeg, xBeg);
     nextBlockWin = newwin(4,6,yBeg+1,(xMax/2)+boardWidth+5);
     mvprintw(yBeg,(xMax/2)+boardWidth+6,"Next");
     mvprintw(yBeg+7,(xMax/2)+boardWidth+5,"Score: %i", score);
     mvprintw(yBeg+9,(xMax/2)+boardWidth+5,"Level: %i", level);
     refresh();
     uint space = static_cast<uint>(' ');
     wborder(boardWin,0,0,space,0, space, space,0,0);



     AddNextBlock();


     bool gameOver = false;

     while(!gameOver)
     {
         //timer
         using namespace std::literals::chrono_literals;
         std::this_thread::sleep_for(50ms);
         //input

         //logic

         //rendering
         PrintNextBlock();
         PrintBoard();
         wrefresh(nextBlockWin);
         wrefresh(boardWin);
     }
/*

     //    fallingBlockWin = newwin(4,4,yBeg-3+CurrentBlock.get_y_loc(),(xMax/2)+CurrentBlock.get_x_loc());
     //wattron(fallingBlockWin,A_INVIS);


     //CurrentBlock.RotateRight();
     //CurrentBlock.RotateRight();
     PrintNextBlock();
     PrintBoard();
   //  PrintFallingBlock();

     refresh();
     wrefresh(nextBlockWin);
     //wrefresh(fallingBlockWin);

         size_t yVar = CurrentBlock.get_y_loc()+1;
         size_t xVar = CurrentBlock.get_x_loc();
 //    while(yVar < fallingBoard->GetHeight())

     for(int i = 0; i < 8; i++)
     {
    //     if(!check_can_move(yVar, xVar, RIGHT))
      //           break;
       //  if(!check_can_move(static_cast<int>(yVar), static_cast<int>(xVar), DOWN))
        //     break;
         Board->MoveBlock(CurrentBlock, DOWN);
         refresh();
         PrintBoard();
        // PrintFallingBlock();

         wrefresh(boardWin);
         yVar++;

     }


  //  Board->RotateBlock(CurrentBlock);
   // Board->RotateBlock(CurrentBlock);
    Board->RotateBlock(CurrentBlock);

 //    CurrentBlock.RotateRight();
   // CurrentBlock.RotateRight();
     //CurrentBlock.RotateRight();

    Board->SetBlock(CurrentBlock);
     PrintBoard();
     wrefresh(boardWin);
     PrintBoard();

     for(int i = 0; i < 20 ; i++)
     {
    //     if(!check_can_move(yVar, xVar, RIGHT))
      //           break;
       //  if(!check_can_move(static_cast<int>(yVar), static_cast<int>(xVar), DOWN))
        //     break;
         Board->MoveBlock(CurrentBlock, DOWN);
         refresh();
         PrintBoard();
        // PrintFallingBlock();

         wrefresh(boardWin);
         yVar++;

     }
     for(int i = 0; i < 17; i++)
     {
    //     if(!check_can_move(yVar, xVar, RIGHT))
      //           break;
       //  if(!check_can_move(static_cast<int>(yVar), static_cast<int>(xVar), DOWN))
        //     break;

         Board->MoveBlock(CurrentBlock, RIGHT);
         refresh();
         PrintBoard();
        // PrintFallingBlock();

         wrefresh(boardWin);
         yVar++;

     }
     PrintBoard();
     wrefresh(boardWin);
     PrintBoard();
     PrintBoard();
     for(int i = 0; i < 17; i++)
     {
    //     if(!check_can_move(yVar, xVar, RIGHT))
      //           break;
       //  if(!check_can_move(static_cast<int>(yVar), static_cast<int>(xVar), DOWN))
        //     break;
         Board->MoveBlock(CurrentBlock, LEFT);

         refresh();
         PrintBoard();
        // PrintFallingBlock();

         wrefresh(boardWin);
         yVar++;

     }

     PrintBoard();
     wrefresh(boardWin);
     PrintBoard();
     Board->RotateBlock(CurrentBlock);

     PrintBoard();
     wrefresh(boardWin);
     PrintBoard();
     Board->RotateBlock(CurrentBlock);

     PrintBoard();
     wrefresh(boardWin);
     PrintBoard();
     Board->RotateBlock(CurrentBlock);

     PrintBoard();
     wrefresh(boardWin);
     PrintBoard();

     /* xVar--;

     for(int i = 0; i < 6; i++)
     {
     //     if(!check_can_move(yVar, xVar, RIGHT))
      //           break;

         fallingBoard->MoveBlock(CurrentBlock, LEFT);
         refresh();
         PrintBoard();
        // PrintFallingBlock();

         wrefresh(boardWin);
         xVar--;

     }

*/

     wgetch(boardWin);
     delwin(boardWin);
     delwin(nextBlockWin);

}

int Game::GetRand()
{
    std::random_device rd;
    std::mt19937 eng(rd());
    std::uniform_int_distribution<int> dist(0,6);
    return dist(eng);
}

bool Game::check_can_move(int y, int x, direction dir)
{

return true;
}

void Game::AddNextBlock()
{
    size_t startY = 0;
    size_t halfWidth = ((Board->GetWidth()-1)/2);
    CurrentBlock = BlockClass(NextBlock.GetId());
    NextBlock = BlockClass(GetRand());
    CurrentBlock.set_x_loc(halfWidth);
    CurrentBlock.set_y_loc(startY);

    Board->SpawnBlock(CurrentBlock, startY, halfWidth);

}

void Game::PrintBoard()
{
    size_t boardHeight = Board->GetHeight();
    size_t boardWidth = Board->GetWidth();

    for(size_t i = 4; i < boardHeight; i++)
    {
        for(size_t y = 0; y < boardWidth; y++)
        {
            char boardTile = Board->GetTile(i,y);
            int curI = static_cast<int>(i);
            int curY = static_cast<int>(y);
            switch(boardTile)
            {
                case ' ':
                    mvwprintw(boardWin, curI-3,curY+1, " ");
                    break;
                case 'I':
                    use_default_colors();
                    start_color();

                    init_pair(1, COLOR_CYAN, -1);

                    wattron(boardWin, COLOR_PAIR(1));
                    mvwprintw(boardWin, curI-3,curY+1, "I");
                    wattroff(boardWin, COLOR_PAIR(1));
                    break;
                case 'J':
                use_default_colors();
                start_color();

                init_pair(2, COLOR_BLUE, -1);

                wattron(boardWin, COLOR_PAIR(2));
                    mvwprintw(boardWin, curI-3,curY+1, "J");
                wattroff(boardWin, COLOR_PAIR(2));
                    break;
                case 'L':
                use_default_colors();
                start_color();

                init_pair(3, COLOR_MAGENTA, -1);

                wattron(boardWin, COLOR_PAIR(3));
                    mvwprintw(boardWin, curI-3,curY+1, "L");
                    wattroff(boardWin,COLOR_PAIR(3));
                    break;
                case 'O':
                use_default_colors();
                start_color();

                init_pair(4, COLOR_YELLOW, -1);

                wattron(boardWin, COLOR_PAIR(4));
                    mvwprintw(boardWin, curI-3,curY+1, "O");
                    wattroff(boardWin, COLOR_PAIR(4));
                    break;
                case 'S': 
                use_default_colors();
                start_color();

                init_pair(5, COLOR_GREEN, -1);

                wattron(boardWin, COLOR_PAIR(5));
                    mvwprintw(boardWin, curI-3,curY+1, "S");
                    wattroff(boardWin,COLOR_PAIR(5));
                    break;
                case 'T':
                use_default_colors();
                start_color();

                init_pair(6, COLOR_WHITE, -1);

                wattron(boardWin, COLOR_PAIR(6));
                    mvwprintw(boardWin, curI-3,curY+1, "T");
                    wattroff(boardWin,COLOR_PAIR(6));
                    break;
                case 'Z':
                use_default_colors();
                start_color();

                init_pair(7, COLOR_RED, -1);

                wattron(boardWin, COLOR_PAIR(7));
                    mvwprintw(boardWin, curI-3,curY+1, "Z");
                    wattroff(boardWin, COLOR_PAIR(7));
                    break;
            }
        }

    }
}

void Game::PrintNextBlock()
{

    wclear(nextBlockWin);
    box(nextBlockWin,0,0);
    int sizeX = static_cast<int>(NextBlock.get_x_size());
    int sizeY = static_cast<int>(NextBlock.get_y_size());
    int ctr = 0;

     for(int i=0; i< sizeY; i++)
    {
        for(int y=0;y<sizeX;y++)
        {
             char curTile = NextBlock.GetTile(static_cast<size_t>(i),
                                              static_cast<size_t>(y));
                switch(curTile)
                {
                    case ' ':
                        mvwprintw(nextBlockWin, i+1,y+1, " ");
                        break;
                    case 'I':
                        ctr++;
                        use_default_colors();
                        start_color();

                        init_pair(1, COLOR_CYAN, -1);

                        wattron(nextBlockWin, COLOR_PAIR(1));
                        mvwprintw(nextBlockWin, i+1,y+1, "I");
                        wattroff(nextBlockWin, COLOR_PAIR(1));
                    break;
                    case 'J':
                    ctr++;
                    use_default_colors();
                    start_color();

                    init_pair(2, COLOR_BLUE, -1);

                    wattron(nextBlockWin, COLOR_PAIR(2));
                        mvwprintw(nextBlockWin, i+1,y+1, "J");
                    wattroff(nextBlockWin, COLOR_PAIR(2));
                        break;
                    case 'L':
                    ctr++;
                    use_default_colors();
                    start_color();

                    init_pair(3, COLOR_MAGENTA, -1);

                    wattron(nextBlockWin, COLOR_PAIR(3));
                        mvwprintw(nextBlockWin, i+1, y+1, "L");
                        wattroff(nextBlockWin,COLOR_PAIR(3));
                    break;
                    case 'O':
                    ctr++;
                    use_default_colors();
                    start_color();

                    init_pair(4, COLOR_YELLOW, -1);

                    wattron(nextBlockWin, COLOR_PAIR(4));
                        mvwprintw(nextBlockWin, i+1, y+1, "O");
                        wattroff(nextBlockWin, COLOR_PAIR(4));
                        break;
                    case 'S':
                    ctr++;
                    use_default_colors();
                    start_color();

                    init_pair(5, COLOR_GREEN, -1);

                    wattron(nextBlockWin, COLOR_PAIR(5));
                        mvwprintw(nextBlockWin, i+1,y+1, "S");
                        wattroff(nextBlockWin,COLOR_PAIR(5));
                        break;
                    case 'T':
                    ctr++;
                    use_default_colors();
                    start_color();

                    init_pair(6, COLOR_WHITE, -1);

                    wattron(nextBlockWin, COLOR_PAIR(6));
                        mvwprintw(nextBlockWin, i+1,y+1, "T");
                        wattroff(nextBlockWin,COLOR_PAIR(6));
                        break;
                    case 'Z':
                    ctr++;
                    use_default_colors();
                    start_color();

                    init_pair(7, COLOR_RED, -1);

                    wattron(nextBlockWin, COLOR_PAIR(7));
                        mvwprintw(nextBlockWin, i+1,y+1, "Z");
                        wattroff(nextBlockWin, COLOR_PAIR(7));
                        break;
                    default:
                        break;
               }
        }
        if(ctr == 4)
            break;
    }

}

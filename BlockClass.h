#ifndef _BLOCKCLASS_H
#define _BLOCKCLASS_H
#include <vector>
#include <algorithm>
#include "pieces.h"


enum blockId { I, O, J, L, S, T, Z }; 
enum offsetDir { BOTTOM_OFFSET, LEFT_OFFSET, RIGHT_OFFSET, TOP_OFFSET };

class BlockClass
{
    public:
        //Constructors
        BlockClass();
        BlockClass(int pieceTypeIn);
        BlockClass(const BlockClass &bClassIn);
        ~BlockClass();
        //functions
        
        //sets the shape based on a given char array block
        void SetShape(std::vector<std::vector<char>> shapeIn);
        
        //Gets the shape of a block and returns as char array
        std::vector<std::vector<char>> GetShape();
        
        //gets the rotation state of a block
        int GetRotation();
        
        //gets a random number for the next piece
        int GetRand();
        
        //gets xLoc
        int get_x_loc()const;

        //gets yLoc
        int get_y_loc()const;
        
        //gets X_SIZE
        size_t get_x_size()const;
        //gets Y_SIZE
        size_t get_y_size()const;

        size_t get_first_row()const;

        size_t get_first_col()const;

        size_t get_rows()const;

        size_t get_cols()const;

        int get_offset(offsetDir offsetSide)const;
        std::vector<size_t> get_offset_test(offsetDir offsetSide)const;

        //sets...
        void set_y_loc(int yLocIn);
        void set_x_loc(int xLocIn);

        //returns a tile
        char GetTile(size_t y, size_t x)const;
        
        //returns the blockId
        blockId GetId()const;

        //rotates the block left, in-place rotation
        void RotateLeft();

        //rotates the block right, in-place rotation
        void RotateRight();

        //returns true if floorkick has happened once
        bool GetFloorKickStatus();

        void SetFloorKicked();
        //operator overloads
        BlockClass& operator=(const BlockClass &bClassIn);
    private:
        int xLoc, yLoc;
        bool hasFloorKicked;
        //array to store a block
        //no need for this to be dynamic
        std::vector<std::vector<char>> blockArr;
        std::vector<int> offSets;
        //the rotation state the block is currently in
        int rotState = 0;
        blockId id;
        //seed based on time
//        unsigned seed = std::chrono::steady_clock::now().time_since_epoch().count();
     
};

#endif

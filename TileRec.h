#ifndef TILEREC_H
#define TILEREC_H
//defines a tile on the board
struct TileRec
{
    bool isActive;
    char tile;
    TileRec() { isActive = false; tile = ' '; }
};
#endif // TILEREC_H

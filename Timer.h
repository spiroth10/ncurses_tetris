#ifndef _TIMERS_H
#define _TIMERS_H
#include <chrono>
#include <memory>

void myFunc();

class Timer
{

public:

    static Timer* Instance();
    static void Release();
    void Reset();
    float DeltaTime();
    void SetTimeScale(float t);
    float GetTimeScale();

    void Tick();


private:

    Timer();
    ~Timer();
    static Timer* sInstance;
    std::chrono::steady_clock::time_point mStartTime;
    std::chrono::duration<float> mDeltaTime;
    float mTimeScale;

};

#endif

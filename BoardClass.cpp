#include "BoardClass.h"

BoardClass::BoardClass()
{
    ySize = 24;
    xSize = 10;
    board.resize(ySize, std::vector<TileRec>(xSize));
}

BoardClass::BoardClass(int ySizeIn, int xSizeIn)
{
    ySize = static_cast<size_t>(ySizeIn);
    xSize = static_cast<size_t>(xSizeIn);
    board.resize(ySize, std::vector<TileRec>(xSize));

}

BoardClass::BoardClass(const BoardClass &boardIn)
{
    *this = boardIn;
}

BoardClass::~BoardClass()
{
}

size_t BoardClass::GetWidth()const
{
    return xSize;
}

size_t BoardClass::GetHeight()const
{
    return ySize;
}

char BoardClass::GetTile(size_t y, size_t x)const
{
    return board[y][x].tile;
}

void BoardClass::SetTile(char tile, size_t y, size_t x)
{
    board[y][x].tile = tile;
    board[y][x].isActive = true;
}

void BoardClass::SpawnBlock(BlockClass &block, size_t y , size_t x)
{
    int curX = block.get_x_loc();
    int curY = block.get_y_loc();
    size_t blockSizeY = block.get_y_size();
    size_t blockSizeX = block.get_x_size();

    if(block.GetId() == O)
    {
        x++;
        block.set_x_loc(curX+1);
      //  block.set_y_loc(curY+1);
    }
    if(block.GetId() == I)
    {
        block.set_x_loc(curX-1);
        block.set_y_loc(curY+1);
    }
   if(block.GetId() != I)
    {
       y++;
       block.set_y_loc(curY+1);
    }

    for(size_t i = 0; i < blockSizeY; i++)
    {
        for(size_t n = 0; n < blockSizeX; n++)
        {
            char tile = block.GetTile(i,n);
            if(tile != ' ')
            {
                int yTmp = static_cast<int>(y) + static_cast<int>(i) +curY;
                size_t yLoc = static_cast<size_t>(yTmp);
                int xTmp = static_cast<int>(x) + static_cast<int>(n) +curY;
                size_t xLoc = static_cast<size_t>(xTmp);
                SetTile(tile,yLoc,xLoc);
            }
        }
    }
}

void BoardClass::SetBlock(BlockClass &block)
{
       // int edgeCtrY = 0;
   // int edgeCtrX = 0;
    int curX = block.get_x_loc();
    int curY = block.get_y_loc();
    size_t blockSizeY = block.get_y_size();
    size_t blockSizeX = block.get_x_size();

    for(size_t i = 0; i < blockSizeY; i++)
    {
        for(size_t n = 0; n < blockSizeX; n++)
        {
       //     if((i+curY > ySize-1) || (n+curX > xSize-1))
                //    break;
            char tile = block.GetTile(i,n);
            if(tile != ' ')
            {
            int yTmp = static_cast<int>(curY)+static_cast<int>(i);
            size_t yLoc = static_cast<size_t>(yTmp);
            int xTmp = static_cast<int>(curX)+static_cast<int>(n);
            size_t xLoc = static_cast<size_t>(xTmp);

                SetTile(tile,yLoc,xLoc);
            }
        }
    }


}


void BoardClass::MoveBlock(BlockClass &block, direction dir)
{

    //get current x/y locations
    int curY = static_cast<int>(block.get_y_loc());
    int curX = static_cast<int>(block.get_x_loc());
    bool stuck = false;

    int offset= 0;
    switch(dir)
    {
        case DOWN:
        {
            offset = block.get_offset(BOTTOM_OFFSET);

            if(curY + offset == static_cast<int>(ySize)-1)
                stuck = true;

            if(!stuck)
            {
                RemoveBlock(block);
                block.set_y_loc(curY+1);
                SetBlock(block);
            }
            break;
        }
        case LEFT:
        {
            offset = block.get_offset(LEFT_OFFSET);
            if(curX - offset <= 0)
                stuck = true;

            if(!stuck)
            {
                RemoveBlock(block);
                block.set_x_loc(curX-1);
                SetBlock(block);
            }
            break;
        }
        case RIGHT:
        {
            offset = block.get_offset(RIGHT_OFFSET);
            if(curX + offset  == static_cast<int>(xSize)-1)
                stuck = true;

            if(!stuck)
            {
                RemoveBlock(block);
                block.set_x_loc(curX+1);
                SetBlock(block);
            }

        break;
        }

    }
}

void BoardClass::RotateBlock(BlockClass &block)
{
    //wrap around rotation function here
    //to allow for bounds/collision detection on rotation

    //int origState = block.GetRotation();
    int curX = block.get_x_loc();
    int curY = block.get_y_loc();
    //create temporary copy of block
    //rotate the block right
    BlockClass tmpBlock(block);
    tmpBlock.RotateRight();
    int offsetLeftX = tmpBlock.get_offset(LEFT_OFFSET);
    int offsetRightX = tmpBlock.get_offset(RIGHT_OFFSET);
    int offsetY = tmpBlock.get_offset(BOTTOM_OFFSET);
    bool canRotate = true;

    //wall kick movement cod e will go within these if statements
    //instead of just "cant rotate"
    //if going off the board on the left
    if(curX - offsetLeftX < 0)
    {
        if(curX+1-offsetLeftX < 0)
            canRotate = false;
        else
            tmpBlock.set_x_loc(curX+1);
    }
    //if its off the board to the right
    if(curX + offsetRightX > static_cast<int>(xSize)-1)
    {
        if(curX-1+offsetRightX > static_cast<int>(xSize)-1)
            canRotate = false;
        else
            tmpBlock.set_x_loc(curX-1);
    }
    //if its off the board to the bottom
    if((curY + offsetY > static_cast<int>(ySize)-1) && !tmpBlock.GetFloorKickStatus())
    {
        tmpBlock.SetFloorKicked();
        blockId tempId = tmpBlock.GetId();
        if(tempId != I)
        {
            if(curY-1+offsetY < 0)
                canRotate = false;
            else
                tmpBlock.set_x_loc(curY-1);
        }
        else
        {
            if(curY-1+offsetY >= 0)
            {
           //blank for now writing PieceCanFit     tmpBlock.
            }
        }
    }

    if(canRotate)
    {
        RemoveBlock(block);
        block = tmpBlock;
        SetBlock(block);
    }
/*    else
    {
        while(block.GetRotation() != origState)
            block.RotateRight();
    }*/
}

bool BoardClass::DoesPieceFit(const BlockClass &block, direction dir)
{
    //needs to do collision and bounds checking for rotations only
    // bounds checking for the normal left/right/down is already done in
    //the movement code.


    //int origState = block.GetRotation();
    int curX = block.get_x_loc();
    int curY = block.get_y_loc();
    //create temporary copy of block
    //rotate the block right
    BlockClass tmpBlock(block);
    tmpBlock.RotateRight();
    int offsetLeftX = tmpBlock.get_offset(LEFT_OFFSET);
    int offsetRightX = tmpBlock.get_offset(RIGHT_OFFSET);
    int offsetY = tmpBlock.get_offset(BOTTOM_OFFSET);
    bool canRotate = true;


    return true;
}


void BoardClass::ClearBoard()
{
    TileRec emptyTile;
    for(size_t i = 0; i < ySize; i++)
    {
        for(size_t y =0; y < xSize; y++)
        {
            board[i][y] = emptyTile;
        }
    }
    //board.assign(ySize,std::vector<char>(xSize, ' '));
}

void BoardClass::RemoveBlock(BlockClass& block)
{
    size_t blockSizeY = block.get_y_size();

    size_t blockSizeX = block.get_x_size();

    int blockLocY = block.get_y_loc();

    int blockLocX = block.get_x_loc();

    for(size_t i = 0; i < blockSizeY; i++)
    {
        for(size_t n=0; n < blockSizeX; n++)
        {
            int yTmp = blockLocY+static_cast<int>(i);
            size_t yLoc = static_cast<size_t>(yTmp);
            int xTmp = blockLocX+static_cast<int>(n);
            size_t xLoc = static_cast<size_t>(xTmp);

            if(yLoc < ySize && xLoc < xSize)
            {
                if(board[yLoc][xLoc].isActive)
                {
                    board[yLoc][xLoc].tile = ' ';
                    board[yLoc][xLoc].isActive = false;
                }
            }
        }
    }
}

void BoardClass::LandBlock(BlockClass &block)
{
    size_t blockSizeY = block.get_y_size();

    size_t blockSizeX = block.get_x_size();

    int blockLocY = block.get_y_loc();

    int blockLocX = block.get_x_loc();

    for(size_t i = 0; i < blockSizeY; i++)
    {
        for(size_t n=0; n < blockSizeX; n++)
        {
            int yTmp = blockLocY+static_cast<int>(i);
            size_t yLoc = static_cast<size_t>(yTmp);
            int xTmp = blockLocX+static_cast<int>(n);
            size_t xLoc = static_cast<size_t>(xTmp);

            if(yLoc < ySize && xLoc < xSize)
            {
                if(board[yLoc][xLoc].isActive)
                {
                    board[yLoc][xLoc].isActive = false;
                }
            }
        }
    }

}

const BoardClass& BoardClass::operator=(const BoardClass &boardIn)
{
            board.resize(boardIn.xSize*boardIn.ySize);
            board = boardIn.board;
            return *this;
}

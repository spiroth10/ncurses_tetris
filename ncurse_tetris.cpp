#include "Game.h"

int start_ncurses(bool useRaw, bool useNoEcho, bool useCBreak, bool hideCursor);
int display_menu();
int start_game();

enum status : signed int {ERROR=-1, QUIT=0,INIT = 1 ,MENU=2, ABOUT=3, GAME=4};

int main()
{
    status programStatus = INIT;
    do
    {
        switch(programStatus)
        {
            case INIT:
                programStatus = static_cast<status>(start_ncurses(false,true,true,true));
                break;
            case MENU:
                programStatus = static_cast<status>(display_menu());
                break;
            case ABOUT:
                programStatus = static_cast<status>(display_menu());
                break;
            case GAME:
                programStatus = static_cast<status>(start_game());
                break;
            default:
            //this is where an error message would be
                break;
        }
    }while(programStatus > QUIT); //this the end condition which is returned from whatever function is executed when the program quits
    endwin();
    return programStatus;
}

int start_ncurses(bool useRaw, bool useNoEcho, bool useCBreak, bool hideCursor)
{
    initscr();
    if(useRaw)
        raw();
    if(useNoEcho)
        noecho();
    if(useCBreak)
        cbreak();
    if(hideCursor)
        curs_set(0);
    return MENU;
}

int display_menu()
{
    int height, width, yMax, xMax;
    int highlight = 0;
    int choice = 0;
    getmaxyx(stdscr,yMax,xMax);
    height = 10;
    width = 20;
    WINDOW * menuWin;
    WINDOW * titleWin;
    titleWin = newwin(height, width, (yMax/2)-5, (xMax/2)-5);
    menuWin = newwin(height, width, yMax/2, (xMax/2) - 8);
    wprintw(titleWin, "Ncurses Tetris");
    wrefresh(titleWin);
    keypad(menuWin, true);
    box(menuWin, 0, 0);
     
   while (true)
   {
        mvwprintw(menuWin, 1, 6, "|-Menu-|");
        if(highlight == 0)
            wattron(menuWin, A_REVERSE);
        mvwprintw(menuWin,3,5, "Start Game");
        if(highlight == 0)
            wattroff(menuWin, A_REVERSE);
        if(highlight == 1)
            wattron(menuWin, A_REVERSE);
        mvwprintw(menuWin,5,5, "About Game");
        if(highlight == 1)
            wattroff(menuWin, A_REVERSE);
        if(highlight == 2)
            wattron(menuWin, A_REVERSE);
        mvwprintw(menuWin,7,5, " Exit Game");
        if(highlight == 2)
            wattroff(menuWin, A_REVERSE);
        choice = wgetch(menuWin);
        switch(choice)
        {
            case KEY_UP:
                highlight--;
                if(highlight == -1)
                    highlight = 0;
                break;
            case KEY_DOWN:
                highlight++;
                if(highlight == 3)
                    highlight = 2;
                break;
            case 10:
                switch(highlight)
                {
                    case 0:
                        wclear(menuWin);
                        wclear(titleWin);
                        wrefresh(menuWin);
                        wrefresh(titleWin);
                        delwin(menuWin);
                        delwin(titleWin);
                        return GAME;
                    case 1:
                        printw("This will lead to an about function!");
                        getch();
                        clear();
                        refresh();
                        return ABOUT;
                    case 2:
                        wclear(menuWin);
                        wclear(titleWin);
                        wrefresh(menuWin);
                        wrefresh(titleWin);
                        printw("Are you sure you want to leave?(Y/N): ");
                        int ans;
                            ans = getch();
                            ans = std::toupper(ans);
                            mvprintw(0,38,"%c", ans);
                            getch();
                            switch(ans)
                            {
                                case 'Y':
                                    return QUIT;
                                default:
                                    clear();
                                    refresh();
                                    return MENU;
                            }
                            delwin(menuWin);
                            delwin(titleWin);

                }
                break;
            default:
                break;
        }
   }
   delwin(menuWin);
   delwin(titleWin);
   return ERROR;
}

int start_game()
{
    Game TetrisGame(10,24);
    TetrisGame.StartGame();
    return QUIT;
}

#include "BlockClass.h"

BlockClass::BlockClass()
{
    //sizeX=0;
    //sizeY=0;
    rotState = 0;
    yLoc = 0;
    hasFloorKicked = false;
}

BlockClass::BlockClass(int pieceTypeIn)
{
//    sizeX=3;
  //  sizeY=3;
    //set initial rotation state
    hasFloorKicked = false;
    rotState = 0;
    switch(pieceTypeIn)
    {
        case I: 
    //        sizeX=4;
      //      sizeY=4;
            //yLoc = 1;
           // blockArr.resize(sizeY, std::vector<char>(sizeX, ' '));
            blockArr = I_PIECE;
            id = I;
            break;
        case O:
        //    sizeX=2;
          //  sizeY=2;
           // blockArr.resize(sizeY, std::vector<char>(sizeX, ' '));
            blockArr = O_PIECE;
            id = O;
            break;
        case J: 
           // blockArr.resize(sizeY, std::vector<char>(sizeX, ' '));
            blockArr = J_PIECE;
            id = J;
            break;
        case L: 
       // blockArr.resize(sizeY, std::vector<char>(sizeX, ' '));
            blockArr = L_PIECE;
            id = L;
            break;
        case S: 
        //blockArr.resize(sizeY, std::vector<char>(sizeX, ' '));
        blockArr = S_PIECE;
            id = S;
            break;
        case T: 
        //blockArr.resize(sizeY, std::vector<char>(sizeX, ' '));
        blockArr = T_PIECE;
            id = T;
            break;
        case Z: 
        //blockArr.resize(sizeY, std::vector<char>(sizeX, ' '));
        blockArr = Z_PIECE;
            id = Z;
            break;
        //default case should never happen as the distribution is only between 0-6
        //may add error message later... but it wont happen without messing up the code...
        default: 
            break;
    }
    size_t offsetId;
    switch(id)
    {
        case I:
            offsetId = 0;
        break;
        case O:
            offsetId = 1;
        break;
        default:
            offsetId = 2;
        break;
    }

    //set the offsets
    for(size_t i = 0; i < 4; i++)
    {
        int val = OFFSETS[offsetId][i];
        offSets.push_back(val);
    }
}

BlockClass::BlockClass(const BlockClass &bClassIn)
{
    *this = bClassIn; 
}

BlockClass::~BlockClass()
{
}

void BlockClass::SetShape(std::vector<std::vector<char>> shapeIn)
{
    //this function relies on the shapeIn being an array 
    //the same size as one of the shapes (4x4) and sets the
    //current shape to that
    blockArr = shapeIn;
}

std::vector<std::vector<char>> BlockClass::GetShape()
{
    return blockArr;
}

int BlockClass::GetRotation()
{
    return rotState;
}

char BlockClass::GetTile(size_t y, size_t x)const
{
    return blockArr[y][x];
}

void BlockClass::RotateRight()
{
   if(id != O)
    {
    if (rotState < 3)
        rotState++;
    else
        rotState = 0;
    /*
    size_t oldY = get_first_row();


    size_t oldX = get_first_col();

*/
    size_t sizeY = get_y_size();
    size_t sizeX = get_x_size();

    for(size_t x = 0; x < sizeY/2; x++)
    {
        for(size_t y = x; y < sizeX -x - 1; y++)
        {
            char temp = blockArr[sizeY-1-x][sizeX-1-y];
            blockArr[sizeY-1-x][sizeX-1-y] = blockArr[y][sizeX - 1 - x];
            blockArr[y][sizeX - 1 - x] = blockArr[x][y];
            blockArr[x][y] = blockArr[sizeY - 1 - y][x];
            blockArr[sizeX - 1 - y][x] = temp;
        }
    }
    //rotate the offsets to to the right to fit the piece rotation
    std::rotate(offSets.rbegin(), offSets.rbegin() + 1, offSets.rend());

}
}
    /*
    size_t newY = get_first_row();

    size_t newX = get_first_col();


    if(newX > oldX)
    {
        xLoc++;
    }
    else if (newX < oldX)
    {
        xLoc--;
    }

    if(newY > oldY)
    {
        yLoc++;
    }
    else if(newY < oldY)
    {

        yLoc--;
    }
    */
//}
/*
void BlockClass::RotateLeeft()
{
    if(rotState < 4)
        rotState++;
    else
        rotState = 0;
    for(int x = 0; x < Y_SIZE/2; x++)
    {
        for(int y = x; y < X_SIZE-x-1; y++)
        {
            char temp = blockArr[Y_SIZE-1-x][X_SIZE-1-y];
            blockArr[4-1-x][4-1-y] = blockArr[y][4 - 1 - x]; 
            blockArr[y][4 - 1 - x] = blockArr[x][y];
            blockArr[x][y] = blockArr[4 - 1 - y][x];
            blockArr[4 - 1 - y][x] = temp;
        }
    }
}
*/
int BlockClass::get_x_loc()const
{
    return xLoc;
}

int BlockClass::get_y_loc()const
{
    return yLoc;
}
/*
size_t BlockClass::get_first_row()const
{
    size_t blockX = 0;
    bool foundX = false;
    for(size_t i=0; i < sizeY; i++)
    {
        for(size_t y=0; y < sizeX; y++)
        {
            if(i+y >= sizeY)
                break;
            if(blockArr[i][y] != ' ')
            {
                foundX = true;
                blockX = i;
                break;
            }
        }
        if(foundX)
            break;
    }
    return blockX;
}

size_t BlockClass::get_first_col()const
{
    bool foundY = false;
    size_t blockY = 0;
    size_t ctr = 0;
    while(!foundY)
    {

    for(size_t i=0; i < sizeY; i++)
    {
        if(blockArr[i][ctr] != ' ')
        {
            blockY = ctr;
            foundY = true;
            break;
        }
        if(foundY)
            break;
    }
    ctr++;
    }
    return blockY;
}
*/
size_t BlockClass::get_x_size()const
{
    return blockArr[0].size();
}

size_t BlockClass::get_y_size()const
{
    return blockArr.size();
}

void BlockClass::set_x_loc(int xLocIn)
{
    xLoc = xLocIn;
}

void BlockClass::set_y_loc(int yLocIn)
{
    yLoc = yLocIn;
}

blockId BlockClass::GetId()const
{
    return id;
}
/*
size_t BlockClass::get_rows()const
{


    size_t blockSizeY = get_y_size();
    size_t blockSizeX = get_x_size();
    size_t rows = 0;

    for(size_t i=0; i < blockSizeY; i++)
    {
        for(size_t y=0; y < blockSizeX; y++)
        {
            if(i >= sizeY)
                break;
            if(blockArr[i][y] != ' ')
            {
                rows++;
                break;
            }
        }
    }


    return rows;
}

size_t BlockClass::get_cols()const
{

        size_t blockSizeY = get_y_size();
        size_t blockSizeX = get_x_size();
        size_t cols = 0;
        size_t ctr = 0;
        while(ctr != blockSizeX)
        {

        for(size_t i=0; i < blockSizeY; i++)
        {
            if(blockArr[i][ctr] != ' ')
            {
                cols++;
                break;
            }
        }
        ctr++;
        }

        return cols;

}
*/

int BlockClass::get_offset(offsetDir offsetSide)const
{

    /*
    switch(offsetSide)
    {
    case BOTTOM_OFFSET:
        if((id != I) && (id != O) && (rotState == 2))
            return (offSets[0]+1);
    //    if((id == I) && (rotState == 3))
      //      return (offSets[0]+1);
        if(id == I)
            return OFFSETS_I[static_cast<size_t>(rotState)][0];

        return offSets[0];
    case LEFT_OFFSET:
      //  if((id == I) && (rotState > 0))
       //     return (offSets[1]-1);
        if(id == I)
            return OFFSETS_I[static_cast<size_t>(rotState)][1];

        if((id != I) && (rotState == 3))
            return (offSets[1]+1);
       return offSets[1];
    case TOP_OFFSET:
        return offSets[2];
    case RIGHT_OFFSET:
        if((id != I) && (id != O) && ((rotState == 1)))
            return (offSets[3]+1);
      //  if((id == I) && (rotState == 2))
        //    return (offSets[3]+1);
          if(id == I)
              return OFFSETS_I[static_cast<size_t>(rotState)][3];

        return offSets[3];

    }
    */
    switch(offsetSide)
    {
        case BOTTOM_OFFSET:
            switch(id)
            {
                case I:
                    return OFFSETS_I[static_cast<size_t>(rotState)][0];

                case O:
                    return OFFSETS_O[0];
                default:
                    return OFFSETS_OTHER[static_cast<size_t>(rotState)][0];
            }
        case LEFT_OFFSET:
            switch(id)
            {
                case I:
                    return OFFSETS_I[static_cast<size_t>(rotState)][1];
                case O:
                    return OFFSETS_O[1];
                default:
                    return OFFSETS_OTHER[static_cast<size_t>(rotState)][1];
            }
        case TOP_OFFSET:
        switch(id)
        {
            case I:
                return OFFSETS_I[static_cast<size_t>(rotState)][2];
            case O:
                return OFFSETS_O[2];
            default:
                return OFFSETS_OTHER[static_cast<size_t>(rotState)][2];
        }

        case RIGHT_OFFSET:
            switch(id)
            {
                case I:
                    return OFFSETS_I[static_cast<size_t>(rotState)][3];
                case O:
                    return OFFSETS_O[3];
                default:
                    return OFFSETS_OTHER[static_cast<size_t>(rotState)][3];
            }
    }
}

bool BlockClass::GetFloorKickStatus()
{
    return hasFloorKicked;
}

void BlockClass::SetFloorKicked()
{
    hasFloorKicked = true;
}

BlockClass& BlockClass::operator=(const BlockClass &bClassIn)
{
    id = bClassIn.id;
    xLoc = bClassIn.xLoc;
    yLoc = bClassIn.yLoc;
    hasFloorKicked = bClassIn.hasFloorKicked;
    //copy the rotation state
    rotState = bClassIn.rotState;
    //copy the block from the input class to this class
    blockArr = bClassIn.blockArr;
    offSets = bClassIn.offSets;
    return *this;
}
